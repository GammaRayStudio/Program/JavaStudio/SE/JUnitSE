Java Unit Test Simple Example
======
![cover](assets/readme/cover.png)
專案說明
------
Java 單元測試，簡易參考範例。

+ 測試 JUnit 4 單元測試框架，「特性」與「使用方法」。

模組說明
------

### Java 主程式
dir: src/enoxs/com/
+ junit/
    + S1_AnnotationTest ： 註解測試
    + S2_AssertTest : 斷言測試
    + S3_Hamcrest : 匹配器框架測試
    + S4_IgnoreTest : 忽略註解測試
    + S5_SuiteTest : 套件測試
    + S6_TimeTest : 時間測試
    + S7_ExceptionTest : 例外測試
    
### 使用手冊 (doc)
dir: doc/enoxs/com/
+ util/

        相同目錄結構。
        說明描述 JUnit 4 元件類別特性、使用方法、注意事項...等。
    + [S1_AnnotationTest](doc/enoxs/com/junit/S1_AnnotationTest.md)
    + [S2_AssertTest](doc/enoxs/com/junit/S2_AssertTest.md)
    + [S3_Hamcrest](doc/enoxs/com/junit/S3_HamcrestTest.md)
    
操作環境
------
+ IDE : IntelliJ
+ SDK : OpenJDK8U

依賴
------
dir: lib/
+ junit-4.12.jar
+ hamcrest-all-1.3.jar
    
      此專案不需引用 hamcrest-core-1.3.jar 
      用於供其他專案需 JUnit 簡單功能時備用來源，
      若需完整使用 hamcrest 功能，仍需導入 hamcrest-all-1.3.jar。
