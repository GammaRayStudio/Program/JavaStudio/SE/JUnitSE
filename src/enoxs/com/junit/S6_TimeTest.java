package enoxs.com.junit;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class S6_TimeTest {

    @Test(timeout = 500)
    public void testFailTimeOut() {
        boolean isTrue = true;
        try {
            Thread.sleep(550);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertTrue(isTrue);
    }

    @Test(timeout = 500)
    public void testPassOnTime() {
        boolean isTrue = true;
        try {
            Thread.sleep(450);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertTrue(isTrue);
    }

    @Test
    public void testCount() {
        int len = 100000;
        long count = 0 ;
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                count ++;
            }
        }
        assertTrue(count > 0 );
    }
}
