package enoxs.com.junit;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class S1_AnnotationTest {

    //execute only once, in the starting
    @BeforeClass
    public static void beforeClass() {
        System.out.println("#1. in before class");
    }

    //execute only once, in the end
    @AfterClass
    public static void  afterClass() {
        System.out.println("#5. in after class");
    }

    //execute for each test, before executing test
    @Before
    public void before() {
        System.out.println("#2. in before");
    }

    //execute for each test, after executing test
    @After
    public void after() {
        System.out.println("#4. in after");
    }

    //execute test case1
    @Test
    public void test() {
        System.out.println("#3. in test");
    }

    //execute test case2
    @Test
    public void testEquals() {
        System.out.println("#3. in testEquals");
        assertEquals("Hello World", "Hello World");
    }

}