package enoxs.com.junit;

import org.junit.Test;

import static org.junit.Assert.*;

public class S2_AssertTest {

    @Test
    public void testEquals() {
        String expect = "Test";
        String actual = "Test";
        assertEquals(expect, actual);
    }

    @Test
    public void testNotEquals() {
        String expect = "Test";
        String actual = "Text";
        assertNotEquals(expect, actual);
    }

    @Test
    public void testTrue() {
        boolean isTrue = true;
        assertTrue(isTrue);

    }

    @Test
    public void tesFalse() {
        boolean isFalse = false;
        assertFalse(isFalse);
    }

    @Test
    public void testNull() {
        String empty = null;
        assertNull(empty);
    }

    @Test
    public void testNotNull() {
        String value = "test";
        assertNotNull(value);
    }

    @Test
    public void testSame() {
        String expect = "Test";
        String actual = "Test";
        assertSame(expect, actual);
    }

    @Test
    public void testNotSame() {
        String expect = new String("Test");
        String actual = new String("Test");
        assertNotSame(expect, actual);
    }

    @Test
    public void testArrayEquals() {
        String[] expects = {"Test", "Text"};
        String[] actuals = {"Test", "Text"};
        assertArrayEquals(expects, actuals);
    }

}
