package enoxs.com.junit;

import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.AnyOf.anyOf;
import static org.hamcrest.core.IsNot.not;


public class S3_HamcrestTest {

    @Test
    public void testThatString() {
        String expect, actual;
        actual = "Test-Text";

        expect = "Test";
        assertThat(actual, startsWith(expect));

        expect = "Text";
        assertThat(actual, endsWith(expect));

        expect = "-";
        assertThat(actual, containsString(expect));
    }

    @Test
    public void testThatList() {
        List<Integer> lstNumber = new LinkedList<Integer>(){{
            add(1);
            add(2);
            add(3);
            add(4);
            add(5);
        }};

        assertThat(lstNumber,contains(1,2,3,4,5));
        assertThat(lstNumber,hasItems(1));
        assertThat(lstNumber,hasSize(5));
        assertThat(lstNumber,containsInAnyOrder(5,3,1,2,4));
    }

    @Test
    public void testThatMap(){
        Map<Integer,String> map = new HashMap<Integer, String>(){{
            put(1,"Test");
            put(2,"Text");
        }};

        assertThat(map,hasKey(1));
        assertThat(map,hasValue("Test"));
        assertThat(map,hasEntry(1,"Test"));
    }

    @Test
    public void testThatCondition(){
        String expect01,expect02, actual;
        actual = "Test-Text";

        expect01 = "Test";
        expect02 = "Text";

        assertThat(actual,
                allOf(startsWith(expect01),containsString(expect02)));

        assertThat(actual,
                anyOf(startsWith(expect01),startsWith(expect02)));

        assertThat(actual,
                not(startsWith(expect02)));
    }
}
