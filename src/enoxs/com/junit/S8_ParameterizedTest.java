package enoxs.com.junit;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class S8_ParameterizedTest {

    public static class VerifyUtil {
        public Boolean isLargeThan3(final Integer num){
            return num > 3;
        }
    }

    private static VerifyUtil verifyUtil;
    private Integer inputNumber;
    private Boolean expectedResult;

    @BeforeClass
    public static void initialize() {
        verifyUtil = new VerifyUtil();
    }

    public S8_ParameterizedTest(Integer inputNumber, Boolean expectedResult) {
        this.inputNumber = inputNumber;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection inputNumber() {
        return Arrays.asList(new Object[][] {
                { 0, false },
                { 1, false },
                { 3, false },
                { 5, true },
                { 10, true }
        });
    }

    @Test
    public void testPrimeNumberChecker() {
        System.out.println("Parameterized Number is : " + inputNumber);
        System.out.println("ExpectedResult : " + expectedResult);

        assertEquals(expectedResult,
                verifyUtil.isLargeThan3(inputNumber));
    }
}
