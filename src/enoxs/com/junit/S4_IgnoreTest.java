package enoxs.com.junit;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

@Ignore
public class S4_IgnoreTest {
    @Test
    public void testEquals() {
        String expect = "Test";
        String actual = "Test";
        assertEquals(expect, actual);
    }

    @Test
    public void testTrue() {
        boolean isTrue = true;
        assertTrue(isTrue);

    }

    @Ignore
    @Test
    public void tesFalse() {
        boolean isFalse = false;
        assertFalse(isFalse);
    }
}
