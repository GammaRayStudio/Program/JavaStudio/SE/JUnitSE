package enoxs.com.junit;

import org.junit.Test;

public class S7_ExceptionTest {

    @Test(expected = NumberFormatException.class)
    public void testNumberFormatException() {
        Integer.parseInt("Test");
    }

    @Test(expected = ArithmeticException.class)
    public void testPrintMessage() {
        Integer number = 1/0;
    }
}
