package enoxs.com.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        S1_AnnotationTest.class,
        S2_AssertTest.class,
        S4_IgnoreTest.class
})
public class S5_SuiteTest {
}
