package enoxs.com.reflection.component;

import java.util.List;

public interface AppService {
    Integer calSum(Integer num1, Integer num2);
    Boolean isEmpty(List<Object> lstObj);
    Boolean isPublicMethodComeBackTrue();
}
