package enoxs.com.reflection.component;

import java.util.List;

public class AppServiceImpl implements AppService {
    public Boolean isPublicMethodComeBackTrue(){
        return true;
    }

    @Override
    public Integer calSum(Integer num1, Integer num2) {
        return num1 + num2;
    }

    @Override
    public Boolean isEmpty(List<Object> lstObj) {
        return lstObj.size() == 0 ? true : false;
    }

    protected Boolean isProtectedMethodComeBackTrue(){
        return true;
    }

    private Boolean isPrivateMethodComeBackTrue(){
        return true;
    }

    private Boolean isPrivateMethodWithParameterComeBackTrue(String msg , Integer num , Boolean isStatus){
        StringBuffer sb = new StringBuffer(32);
        sb.append("\nMESSAGE:");
        sb.append(msg);
        sb.append("\nNUMBER:");
        sb.append(num);
        sb.append("\nSTATUS:");
        sb.append(isStatus);
        System.out.println(sb.toString());
        return true;
    }
}
