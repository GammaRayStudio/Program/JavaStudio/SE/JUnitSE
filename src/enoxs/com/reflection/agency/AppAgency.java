package enoxs.com.reflection.agency;

import enoxs.com.reflection.component.AppService;
import enoxs.com.reflection.component.AppServiceImpl;
import enoxs.com.reflection.util.AgencyUtil;
import enoxs.com.util.ReflectionUtil;

import java.util.List;

public class AppAgency extends AgencyUtil implements AppService {
    AppService demoService;

    public AppAgency() {
        this.demoService = new AppServiceImpl();
    }

    @Override
    public Integer calSum(Integer num1, Integer num2) {
        return demoService.calSum(num1, num2);
    }

    @Override
    public Boolean isEmpty(List<Object> lstObj) {
        return demoService.isEmpty(lstObj);
    }

    @Override
    public Boolean isPublicMethodComeBackTrue() {
        return demoService.isPublicMethodComeBackTrue();
    }

    public Boolean isProtectedMethodComeBackTrue(){
        return (Boolean) ReflectionUtil.invokeMethod(demoService, "isProtectedMethodComeBackTrue", null, null);
    }

    public Boolean isPrivateMethodComeBackTrue() {
        return (Boolean) ReflectionUtil.invokeMethod(demoService, "isPrivateMethodComeBackTrue", null, null);
    }

    public Boolean isPrivateMethodWithParameterComeBackTrue(String msg, Integer num, Boolean isStatus) throws ClassNotFoundException {
        initParameters(3);
        setParameterType(0, Class.forName("java.lang.String"));
        setParameterType(1, Class.forName("java.lang.Integer"));
        setParameterType(2, Class.forName("java.lang.Boolean"));//int.class

        setParameter(0, msg);
        setParameter(1, num);
        setParameter(2, isStatus);
        return (Boolean) ReflectionUtil.invokeMethod(demoService,
                "isPrivateMethodWithParameterComeBackTrue", parameterTypes, parameters);
    }
}
