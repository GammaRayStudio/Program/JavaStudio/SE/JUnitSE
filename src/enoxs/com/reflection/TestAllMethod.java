package enoxs.com.reflection;

import enoxs.com.reflection.agency.AppAgency;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestAllMethod {
    private static AppAgency appService;

    @BeforeClass
    public static void beforeClass(){
        appService = new AppAgency();
    }

    @Test
    public void isPublicMethodTrue() {
        Boolean isActual = appService.isPublicMethodComeBackTrue();
        assertTrue(isActual);
    }

    @Test
    public void calSum() {
        Integer num01 = 12;
        Integer num02 = 15;
        Integer expect = 27;
        Integer actual;
        actual = appService.calSum(num01,num02);
        assertEquals(expect,actual);
    }

    @Test
    public void isEmpty() {
        List<Object> lstNum = Arrays.asList(1,3,5,7,9);
        assertFalse(appService.isEmpty(lstNum));
    }

    @Test
    public void isProtectedMethodTrue() {
        assertTrue(appService.isProtectedMethodComeBackTrue());
    }

    @Test
    public void isPrivateMethodTrue(){
        assertTrue(appService.isPrivateMethodComeBackTrue());
    }

    @Test
    public void isPrivateMethodWithParameter(){
        Boolean isActual = null;
        try {
            isActual = appService.isPrivateMethodWithParameterComeBackTrue("Go go go ~ !!!",1,true);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        assertTrue(isActual);
    }
}
