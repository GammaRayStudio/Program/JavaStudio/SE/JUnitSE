package enoxs.com.exec;

import enoxs.com.junit.S5_SuiteTest;
import junit.framework.TestResult;
import junit.framework.TestSuite;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class S3_TestSuite {
    public static void main(String[] a) {
        testSuite01();
        testSuite02();
    }
    private static void testSuite01() {
        // add the test's in the suite
        TestSuite suite = new TestSuite(S1_TestRunner.class, S2_TestCase.class);
        TestResult result = new TestResult();
        suite.run(result);
        System.out.println("Number of test cases = " + result.runCount());
    }

    private static void testSuite02() {
        Result result = JUnitCore.runClasses(S5_SuiteTest.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }
}
