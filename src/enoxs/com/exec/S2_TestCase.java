package enoxs.com.exec;

import junit.framework.TestCase;

public class S2_TestCase extends TestCase {
    protected int value01, value02;

    // assigning the values
    protected void setUp(){
        value01 = 1;
        value02 = 1;
    }

    // test method to add two values
    public void testAdd(){
        double result = value01 + value02;
        System.out.println(value01 + " , " + value02);
        System.out.println(result);

        assertTrue(result == 2);
    }

    public void testEquals() {
        String expect = "Test";
        String actual = "Test";
        assertEquals(expect, actual);
    }
}
