package enoxs.com.exec;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import static org.junit.Assert.assertEquals;

public class S1_TestRunner {

    @Test
    public void testEquals() {
        String expect = "Test";
        String actual = "Test";
        assertEquals(expect, actual);
    }

    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(S1_TestRunner.class);
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }
}
