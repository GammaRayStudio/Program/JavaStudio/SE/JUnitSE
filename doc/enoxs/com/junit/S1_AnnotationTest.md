S1_AnnotationTest
======

功能說明
------
測試 JUnit 4 註解功能特性

檔案路徑
------
+ java: src/enoxs/com/junit/S3_HamcrestTest.java
+ doc : doc/enoxs/com/junit/S3_HamcrestTest.md

功能說明
------

註解：
1. @BeforeClass : 在開始第一個測試前執行
2. @AfterClass : 全部測試完畢後執行
3. @Before : 每次測試前執行
4. @After : 每次測試後執行
5. @Test : 執行區塊內程式碼

依賴：
+ junit-4.12.jar
+ hamcrest-all-1.3.jar