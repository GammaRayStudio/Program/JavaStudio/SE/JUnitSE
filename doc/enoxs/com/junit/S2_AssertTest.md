S2_AssertTest
======

功能說明
------
測試 JUnit 4 斷言功能特性

檔案路徑
------
+ java: src/enoxs/com/junit/S2_AssertTest.java
+ doc : doc/enoxs/com/junit/S2_AssertTest.md

功能說明
------

斷言：

assertEquals(expect,actual)

    判斷 expect 物件 與 actual 物件數值是否相等
    
assertNotEquals(expect,actual)

    判斷 expect 物件 與 actual 物件數值是否不相等
    
assertTrue(isTrue)
    
    判斷 isTrue 布林值是否為 true
    
assertFalse(isFalse)

    判斷 isTrue 布林值是否為 false
    
assertNull(empty)

    判斷 empty 物是否為 null
    
assertNotNull(value)

    判斷 value 物是否不為 null
    
assertSame(expect,actual)

    判斷 expect 物件 與 actual 物件的引用是否相等
    類似於通過 == 和 != 比較兩個物件
     
    assertEquals與 字串比較中使用的 equals()方法類似

assertNotSame(expect, actual)

    判斷 expect 物件 與 actual 物件的引用是否不相等
    
assertArrayEquals

    判斷 expect 陣列 與 actual 陣列的內容是否相等
    
依賴：
+ junit-4.12.jar
+ hamcrest-all-1.3.jar