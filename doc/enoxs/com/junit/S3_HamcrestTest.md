S3_HamcrestTest
======

功能說明
------
測試 JUnit 4 於 4.4 版後引入的 Hamcrest 匹配器框架支援

其目的在於改進斷言測試時的可讀性，閱讀 assertThat()中的斷言，會比較接近閱讀自然語言。

檔案路徑
------
+ java: src/enoxs/com/junit/S1_AnnotationTest.java
+ doc : doc/enoxs/com/junit/S1_AnnotationTest.md

功能說明
------

assertThat:
+ String 
    + assertThat(actual, startsWith(expect))
        + 判斷 actual 字串的是否為起始於 expect 字串
    + assertThat(actual, endsWith(expect))
        + 判斷 actual 字串的是否為結束於 expect 字串
    + assertThat(actual, containsString(expect));
        + 判斷 actual 字串的是否包含 expect 字串
+ List
    + assertThat(lstNumber,contains(1,2,3,4,5));
        + 判斷 lstNumber List 集合是否相等於 (1,2,3,4,5)
    + assertThat(lstNumber,hasItems(1));
        + 判斷 lstNumber List 集合是否有 1 元素
    + assertThat(lstNumber,hasSize(5));
        + 判斷 lstNumber List 集合長度是否為 5
    + assertThat(lstNumber,containsInAnyOrder(5,3,1,2,4));
        + 判斷 lstNumber List 集合是否在任何排序中相等於 (5,3,1,2,4) 
+ Map
    + assertThat(map,hasKey(1));
        + 判斷 map Map 集合是否有 key = 1 元素
    + assertThat(map,hasValue("Test"));
        + 判斷 map Map 集合是否有 value = Test 元素
    + assertThat(map,hasEntry(1,"Test"));
        + 判斷 map Map 集合是否有 (key = 1 & value = Test) 元素
+ Condition
    + assertThat(actual,allOf(startsWith(expect01),containsString(expect02)));
        + 判斷 actual 字串是否全部符合「起始於 expect01 字串」與「包含 expect02 字串」，此二條件。
    + assertThat(actual,anyOf(startsWith(expect01),startsWith(expect02)));
        + 判斷 actual 字串是否任一符合「起始於 expect01 字串」與「起始於 expect02 字串」，此二條件。
    + assertThat(actual,not(startsWith(expect02)));
        + 判斷 actual 字串是否不符合「起始於 expect02 字串」，此一條件。
        
依賴：
+ junit-4.12.jar
+ hamcrest-all-1.3.jar