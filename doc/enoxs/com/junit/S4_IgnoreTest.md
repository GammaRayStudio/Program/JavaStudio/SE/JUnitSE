S4_IgnoreTest
======

功能說明
------
測試 JUnit 4 忽略註解測試(@Ignore)

檔案路徑
------
+ java: src/enoxs/com/junit/S4_IgnoreTest.java
+ doc : doc/enoxs/com/junit/S4_IgnoreTest.md

功能說明
------

忽略測試案例(Class)

```java
@Ignore
public class S4_IgnoreTest {
    
}
```
+ S5_SuiteTest 運行

忽略測試函式(Function)

```java
public void testEquals() {
            String expect = "Test";
            String actual = "Test";
            assertEquals(expect, actual);
}
```


依賴：
+ junit-4.12.jar
+ hamcrest-all-1.3.jar