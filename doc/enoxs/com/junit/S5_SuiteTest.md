S5_SuiteTest
======

功能說明
------
測試 JUnit 4 套件測試（Suite Test），驗證多個案例並指定順序。

檔案路徑
------
+ java: src/enoxs/com/junit/S5_SuiteTest.java
+ doc : doc/enoxs/com/junit/S5_SuiteTest.md

功能說明
------

```java
@RunWith(Suite.class)
@Suite.SuiteClasses({
        S1_AnnotationTest.class,
        S2_AssertTest.class,
        S4_IgnoreTest.class
})
public class S5_SuiteTest {
}
```

測試 S1_AnnotationTest 、 S2_AssertTest 、 S4_IgnoreTest 案例
並且依照順序執行，S4_IgnoreTest 因有 @Ignore 註解，略過。

依賴：
+ junit-4.12.jar
+ hamcrest-all-1.3.jar